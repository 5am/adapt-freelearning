# adapt-freelearning

**adapt-freelearning** is a custom theme adapted from the [**Vanilla**](https://github.com/adaptlearning/adapt-contrib-vanilla/wiki) core [**Adapt**](https://adaptlearning.org) theme. Visit the [**Vanilla** wiki](https://github.com/adaptlearning/adapt-contrib-vanilla/wiki) for more information about its functionality and key properties.

**adapt-freelearning** was developed to be used with my own Adapt [freelearning courses](https://samhowell.uk/freelearning.html). As the name may suggest, my courses, as well as this theme, are developed in the spirit of free (libre) software and free and open access to education.

## Structure

| Folder/File         | Description  |
| :-------------      |:-------------|
| 📁 js                | JavaScript files on which the theme depends |
| 📁 less              | Location of any [LESS](http://lesscss.org/) based CSS files |
| 📁 less/_defaults          | Location of configuration LESS files |
| 📄 less/_defaults/colors.less | Location of global colour variables   |
| 📁 less/core          | Location of Adapt Framework LESS file styles |
| 📁 less/plugins          | Location of Adapt plugin LESS file styles |
| 📁 less/project          | Location of additional LESS file styles |

## Limitations

No known limitations.
